package iot.interfaccia;

import gnu.io.CommPortIdentifier;

import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

/**
 * Show the serial ports available on the PC
 * 
 * @author aricci
 *
 */
public class ShowSerialPorts {

    public Vector<String> getComPorts() {
        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
        Vector<String> tmp = new Vector<>();
        while (portEnum.hasMoreElements()) {
            CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
            tmp.add(currPortId.getName());
        }
        return new Vector<>(tmp);
        
    }
}