package iot.interfaccia;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class View extends JFrame {
    SerialMonitor sm = new SerialMonitor();
    

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public View() {
        this.setVisible(true);
        JPanel main = new JPanel(new BorderLayout());
        JPanel buttons = new JPanel(new FlowLayout());
        JButton stop = new JButton("Stop");
        JButton incoming = new JButton("Incoming");
        JButton start = new JButton("Start");
        
        JComboBox<String> comboBox = new JComboBox<>(new ShowSerialPorts().getComPorts());
        setContentPane(main);
        buttons.add(comboBox);
        buttons.add(start);
        stop.setEnabled(false);
        buttons.add(stop);
        incoming.setEnabled(false);
        buttons.add(incoming);
        main.add(buttons, BorderLayout.NORTH);
        DefaultListModel<String> listModel = new DefaultListModel<>();
        JList<String> lista = new JList<>(listModel);
        JScrollPane scrolling = new JScrollPane(lista);
        scrolling.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {  
            public void adjustmentValueChanged(AdjustmentEvent e) {  
                e.getAdjustable().setValue(e.getAdjustable().getMaximum());  
            }
        });
        main.add(scrolling, BorderLayout.CENTER);
        this.setSize(500, 300);  
        
        incoming.addActionListener(new ActionListener() {            
            @Override
            public void actionPerformed(ActionEvent e) {
                sm.write('I');
            }
        });
        
        stop.addActionListener(new ActionListener() {            
            @Override
            public void actionPerformed(ActionEvent e) {
                sm.write('S');
            }
        });
        
        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sm.start(comboBox.getSelectedItem().toString(), 9600, listModel);
                stop.setEnabled(true);
                incoming.setEnabled(true);
                start.setEnabled(false);
            }
        });
    }

    public static void main(String[] args) {
        new View();
    }

}
